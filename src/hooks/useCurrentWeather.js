
// Hooks
import { useForecast } from './useForecast';
import { useStore } from './useStore';

export const useCurrentWeather = () => {
    const { data } = useForecast();
    const { selectedDayId } = useStore();


    const currentDayIndex = data.findIndex((day) => day.id === selectedDayId);


    return data[ currentDayIndex ];
};
