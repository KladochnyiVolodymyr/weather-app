import { observer } from 'mobx-react-lite';

// Components
import { CurrentWeather } from './components/CurrentWeather';
import { Head } from './components/Head';
import { Forecast } from './components/Forecast';
import { Filter } from './components/forms/Filter';


// Hooks
import { useForecast, useStore } from './hooks';

// Helpers
import { fetchify } from './helpers';

export const App = observer(() => {
    const { data, isFetched } = useForecast();
    const weatherStore = useStore();
    const filteredDays = weatherStore.filteredDays(data || []);

    return (
        <main>
            { fetchify(isFetched, (
                <>
                    <Filter />
                    {
                        filteredDays.length > 0
                            ? <>
                                <Head />
                                <CurrentWeather />
                                <Forecast /></>
                            : <div className = 'forecast'>
                                <p className = 'message'>According to the given criteria, there are no available days!
                                </p>
                            </div>
                    }

                </>
            )) }

        </main>
    );
});
