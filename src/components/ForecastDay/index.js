// Core
import cx from 'classnames';
import { format, isSameDay } from 'date-fns';
import { useEffect } from 'react';
import { observer } from 'mobx-react-lite';


// Helpers
import { useStore } from '../../hooks';

export const ForecastDay = observer((props) => {
    const {
        temperature, day, type, id,
    } = props;
    const weatherStore = useStore();

    const formatDay = new Date(day);
    const formatDayJSX =  format(formatDay, 'EEEE');
    const isToday = isSameDay(new Date(day), new Date());

    const dayClasses = cx('day', type, {
        selected: id === weatherStore.selectedDayId,
    });

    useEffect(() => {
        if (isToday) {
            weatherStore.setSelectedDayId(id);
        }
    }, [id]);

    const handleClick = () => {
        weatherStore.setSelectedDayId(id);
    };

    return (
        <div className = { dayClasses } onClick = { handleClick }>
            <p>{ formatDayJSX }</p>
            <span>{ temperature }</span>
        </div>
    );
});
