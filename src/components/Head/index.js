import { format } from 'date-fns';
import { observer } from 'mobx-react-lite';
import cx from 'classnames';

// Hooks
import { useCurrentWeather } from '../../hooks';


export const Head = observer(() => {
    const data = useCurrentWeather();

    let formatDay;
    let formatNameDayJSX;
    let formatNumberDayJSX;
    let formatNameMonthJSX;

    if (data?.day) {
        formatDay = new Date(data.day);
        formatNameDayJSX =  format(formatDay, 'EEEE');
        formatNumberDayJSX = format(formatDay, 'd');
        formatNameMonthJSX = format(formatDay, 'MMMM');
    }

    const iconClasses = cx('icon', data?.type);

    return (
        <div className = 'head'>
            <div className = { iconClasses }></div>
            <div className = 'current-date'>
                <p>{ formatNameDayJSX }</p>
                <span> { formatNumberDayJSX } { formatNameMonthJSX }</span>
            </div>
        </div>
    );
});
