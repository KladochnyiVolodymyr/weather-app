import { observer } from 'mobx-react-lite';
import { isSameDay } from 'date-fns';

// Components
import { ForecastDay } from '../ForecastDay';

// Hooks
import { useForecast, useStore } from '../../hooks';

// Helpers
import { fetchify } from '../../helpers';

export const Forecast = observer(() => {
    const { data, isFetched } = useForecast();
    const weatherStore = useStore();
    const filteredDays = weatherStore.filteredDays(data);

    const todayIndex = filteredDays.findIndex((day) => isSameDay(new Date(day.day), new Date()));

    const catchDays = filteredDays.slice(todayIndex === -1 ? 0 : todayIndex, todayIndex + 7);

    if (catchDays.length > 0) {
        weatherStore.setSelectedDayId(catchDays[ 0 ].id);
    }

    const forecastJSX = catchDays.map((day) => <ForecastDay key = { day.id } { ...day } />);

    return (
        <div className = 'forecast'>
            { fetchify(isFetched, forecastJSX) }
        </div>
    );
});
