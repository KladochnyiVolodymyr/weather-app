import { observer } from 'mobx-react-lite';

// Hooks
import { useCurrentWeather } from '../../hooks';

export const CurrentWeather = observer(() => {
    const data = useCurrentWeather();

    return (
        <div className = 'current-weather'>
            <p className = 'temperature'>{ data?.temperature }</p>
            <p className = 'meta'>
                <span className = 'rainy'>% { data?.rain_probability }</span>
                <span className = 'humidity'>% { data?.humidity }</span>
            </p>
        </div>
    );
});
