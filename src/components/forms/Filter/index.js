// Core
import { useForm } from 'react-hook-form';
import { observer } from 'mobx-react-lite';
import { useState } from 'react';
import cx from 'classnames';

// Components
import { Input } from '../elements/Input';

// Hooks
import { useStore, useForecast } from '../../../hooks';

export const Filter = observer(() => {
    const [radioValue, setRadioValue] = useState('');
    const [disabledFilter, setDisabledFilter] = useState(true);
    const [disabledForm, setDisabledForm] = useState(false);
    const form = useForm({ mode: 'onChange' });
    const weatherStore = useStore();
    const { data } = useForecast();
    const { isFiltered } = weatherStore;

    const onSubmit = form.handleSubmit((credentials, event) => {
        event.preventDefault();
        weatherStore.applyFilter(credentials);
        weatherStore.filteredDays(data);
        setDisabledForm(true);
    });

    const onReset = (event) => {
        event.preventDefault();
        weatherStore.resetFilter();
        form.reset();
        setRadioValue('');
        setDisabledFilter(true);
        setDisabledForm(false);
    };

    const handleChange = () => {
        const formValues = Object.values(form.getValues());

        if (formValues.find((el) => el !== '')) {
            setDisabledFilter(false);
        } else {
            setDisabledFilter(true);
        }
    };

    const handleCheckbox = (type) => {
        form.setValue('type', type);
        setRadioValue(type);
        handleChange();
    };

    const cloudyClasses = cx('checkbox', {
        selected: radioValue === 'cloudy',
        blocked:  disabledForm,
    });

    const sunnyClasses = cx('checkbox', {
        selected: radioValue === 'sunny',
        blocked:  disabledForm,
    });

    return (
        <form className = 'filter' onChange = { handleChange }>
            <span
                className = { cloudyClasses }
                onClick = { () => handleCheckbox('cloudy') }>Cloudy</span>
            <span
                className = { sunnyClasses }
                onClick = { () => handleCheckbox('sunny') }>Sunny</span>
            <Input
                label = 'MINIMUM TEMPERATURE'
                register = { form.register('minTemperature') }
                type = 'number'
                disabled = { disabledForm } />
            <Input
                label = 'MАXIMUM TEMPERATURE'
                register = { form.register('maxTemperature') }
                type = 'number'
                disabled = { disabledForm } />
            { !isFiltered
                ? <button
                    disabled = { disabledFilter }
                    onClick = { onSubmit }>Filter</button>
                : <button onClick = { onReset }>Reset</button>
            }

        </form>
    );
});
