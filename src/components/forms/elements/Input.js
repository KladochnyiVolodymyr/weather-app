export const Input = (props) => {
    const input = (
        <input
            placeholder = { props.placeholder }
            type = { props.type }
            disabled = { props.disabled }
            { ...props.register } />
    );

    return (
        <p className = 'custom-input'>
            <label>
                { props.label } { ' ' }
                { input }
            </label>
        </p>
    );
};
